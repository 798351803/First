package com.example.textapp;

public class Dish {
	protected String name;
	protected int price;
	protected int count;
	protected boolean isUpload;

	public Dish(String name ,int price,int count) {
		// TODO 自动生成的构造函数存根
		this.name = name;
		this.price = price;
		this.count = count;
		this.isUpload = false;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public boolean isUpload() {
		return isUpload;
	}

	public void setUpload(boolean isUpload) {
		this.isUpload = isUpload;
	}
	
	
	




}
