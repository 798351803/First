package com.example.textapp;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.AvoidXfermode.Mode;

public class DBManager {  
    private DBHelper helper;  
    private SQLiteDatabase db;  
      
    public DBManager(Context context) {  
        helper = new DBHelper(context);  
        db = helper.getWritableDatabase();  
    }  

    public void addIndent(Dish indentdish) {
    	Cursor c = db.rawQuery("SELECT * FROM indent", null);
    	int indent_id =c.getCount()+1;
    	int isUpload = indentdish.isUpload()?1:0;
    	c.close();
    	db.execSQL("INSERT INTO indent VALUES( ?, ?, ?, ?, ?)", new Object[]{indent_id,indentdish.getName(),indentdish.getPrice(),indentdish.getCount(),isUpload});
    }
    public void uploadIndent(){
    	db.u
    	
    	db.execSQL("UPDATA indent SET upload = 1");
    }
    
    public void alterIndent(Dish alterableDish){
    	db.execSQL("Alter indent SET count = ? WHERE upload = 0 AND name = ?",new Object[]{alterableDish.getCount(),alterableDish.getName()});
    }
    
    public ArrayList<Dish> loadIndent() {  
        ArrayList<Dish> dishes = new ArrayList<Dish>();  
        Cursor c = db.rawQuery("SELECT * FROM indent", null);  
        while (c.moveToNext()) {  
        	
            String name		 = c.getString(c.getColumnIndex("name"));  
            int price		 = c.getInt(c.getColumnIndex("price"));
            int count	 	 = c.getInt(c.getColumnIndex("count"));
            boolean isUpload = c.getInt(c.getColumnIndex("isupload"))==1;
            
            Dish dish = new Dish(name,price,count);
            dish.setUpload(isUpload);
            dishes.add(dish);
        }  
        c.close();  
        return dishes;  
    }
    
      

    

    public void closeDB() {  
        db.close();  
    }  
}  